"use strict";

(function () {
  $(document).ready(function () {
    /*===================================================
    Product gallery
    ===================================================*/
    $('.products__img--big').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '.products__img--small',
      arrows: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          arrows: true,
          prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
          nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-chevron-right"></i></button>'
        }
      }]
    });
    $('.products__img--small').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      asNavFor: '.products__img--big',
      focusOnSelect: true,
      variableWidth: true
    });
    $('.list-img').css('display', 'block');
    /*===================================================
    Select Price
    ===================================================*/

    $('.header__price label').on('click', function () {
      $('.header__pricelist').toggle();
    });
    $('.header__pricelist li').on('click', function () {
      var text = $(this).html();
      $('.header__price label').html(text);
      $('.header__pricelist').hide();
    });
    /*===================================================
    Quantity
    ===================================================*/

    $('.quantity-up').on('click', function () {
      var elem = $(this).closest('.quantity').find('.quantity-val');
      var val = Number(elem.val()) + 1;
      elem.val(val);
    });
    $('.quantity-down').on('click', function () {
      var elem = $(this).closest('.quantity').find('.quantity-val');
      var val = Number(elem.val()) - 1;
      if (val < 1) val = 1;
      elem.val(val);
    });
    /*===================================================
    Open + Close Menu SP
    ===================================================*/

    $('.header__bar i').on('click', function () {
      $('.header__naviSp').toggleClass('is-active');
      $('.header__background').toggleClass('is-active');
      $('.close-menu').toggleClass('is-active');
    });
    $('.header__background').on('click', function () {
      $('.header__naviSp').removeClass('is-active');
      $('.header__background').removeClass('is-active');
      $('.close-menu').removeClass('is-active');
    });
    $('.close-menu').on('click', function () {
      $('.header__naviSp').removeClass('is-active');
      $('.header__background').removeClass('is-active');
      $('.close-menu').removeClass('is-active');
    });
    /*===================================================
    Seach
    ===================================================*/

    $('.search').css({
      "display": "none"
    });
    $('.header__searchbtn').on('click', function () {
      $('.search').css({
        "display": "block"
      });
      $('.search').addClass('is-open');
      $('.wrapper').css("overflow", "hidden");
    });
    $('.search .close').on('click', function () {
      $('.search').css({
        "display": "none"
      });
      $('.search').removeClass('is-open');
      $('.wrapper').css("overflow", "inherit");
    });
    /*===================================================
    Reset select color + size + qty
    ===================================================*/

    $('.clearselection').on('click', function (e) {
      e.preventDefault();
      $('.products__row3cl select').val('None');
      $('.products__quantity .quantity-val').val('1');
    });
    /*===================================================
    Click Open and Close box cart
    ===================================================*/

    var cart = $('.cart_bottom').css("display", "none");
    $('.cart_bottom .close').on('click', function () {
      $('.cart_bottom').css({
        'display': 'none'
      });
    });
    $('.checkout .cancel').on('click', function () {
      $('.cart_bottom').css({
        'display': 'none'
      });
    });
    $('.header__car').on('click', function () {
      $('.cart_bottom').css({
        'display': 'block'
      });
    });
    /*===================================================
    Click active wishlist
    ===================================================*/

    $('.checkwish').on('click', function () {
      if ($(this).find('span').html() == 'ADDED TO WISHLISH') {
        $(this).find('span').html('ADD TO WISHLISH');
        $(this).find('i').css('color', 'inherit');
        $('.header__wishlist i').css('color', 'inherit');
      } else {
        $(this).find('span').html('ADDED TO WISHLISH');
        $(this).find('i').css('color', 'red');
        $('.header__wishlist i').css('color', 'red');
      }
    });
    /*===================================================
    Add item in box cart
    ===================================================*/

    var imgSrc;
    var title;
    var color;
    var size;
    var qty;
    var price;
    $('.checkmark').on('click', function () {
      if ($('select[name="Countries"]').val() == "None" || $('select[name="Size"]').val() == "None") {
        alert('Please select color and size !');
      } else {
        imgSrc = $('.slick-track .slick-slide:nth-child(1) .list-img img').attr('src');
        title = $('.titlepro').text();
        color = $('select[name="Countries"]').val();
        size = $('select[name="Size"]').val();
        qty = $('input[name="Quantity"]').val();
        price = parseFloat($('.price').html().replace("$", "")) * $('input[name="Quantity"]').val();
        var li = $('<li></li>');
        var img = $('<div class="img"><img alt=""></div>');
        var name = $('<div class="name"></div>');
        var iDes = $('<div class="i_des"></div>');
        var iClass = $('<div class="i_color"></div>');
        var iSize = $('<div class="i_size"></div>');
        var iQty = $('<div class="i_QTY"></div>');
        var iPrice = $('<div class="i_price"></div>');
        var iRemove = $('<div class="i_remove" deleteIndex="0"><i class="fa fa-times "></i></div>');
        img.find('img').attr('src', imgSrc);
        name.html(title);
        iClass.html(color);
        iSize.html(size);
        iQty.html(qty);
        iPrice.html(price);
        img.appendTo(iDes);
        name.appendTo(iDes);
        iClass.appendTo(iClass);
        iSize.appendTo(iSize);
        iQty.appendTo(iQty);
        iPrice.appendTo(iPrice);
        iDes.appendTo(li);
        iClass.appendTo(li);
        iSize.appendTo(li);
        iQty.appendTo(li);
        iPrice.appendTo(li);
        iRemove.appendTo(li);
        li.appendTo('.cart_list .list ul'); // Number cart item

        $(".total-count").text($(".cart_list .list ul li").length); // Number total

        var totalPrice = 0;
        $(".i_price").each(function () {
          var pricePerItem = parseFloat($(this).text().replace("$", ""));
          totalPrice += pricePerItem;
        });
        $(".totalP").text(totalPrice); // Number tax

        var ttprice = $(".totalP").text();
        $(".taxP").text((ttprice / 100 * 10).toFixed(2)); // Number total and tax

        var tax = parseFloat($(".taxP").text());
        var ttpriceNumber = parseFloat($(".totalP").text());
        var sumTV = tax + ttpriceNumber;
        $(".totalTaxP").text(sumTV); // Remove cart item 

        $('.i_remove i').click(function () {
          $(this).parent().parent().remove(); // Number total

          var totalPrice = 0;
          $(".i_price").each(function () {
            var pricePerItem = parseFloat($(this).text().replace("$", ""));
            totalPrice += pricePerItem;
          });
          $(".totalP").text(totalPrice); // Number tax

          var ttprice = $(".totalP").text();
          $(".taxP").text((ttprice / 100 * 10).toFixed(2)); // Number total and tax

          var tax = parseFloat($(".taxP").text());
          var ttpriceNumber = parseFloat($(".totalP").text());
          var sumTV = tax + ttpriceNumber;
          $(".totalTaxP").text(sumTV);
          $(".total-count").text($(".cart_list .list ul li").length);
        });
      }
    });
    $(".total-count").text($(".cart_list .list ul li").length);
    /*===================================================
    Number item in wishlist
    ===================================================*/

    var numberitemW = $('.products__wishlist table tbody tr').length;
    $('.products__wishlist .title .number-wish').text(numberitemW); // Remove cart item 

    $('.products-remove i').click(function () {
      $(this).parent().parent().remove();
      var numberitemW = $('.products__wishlist table tbody tr').length;
      $('.products__wishlist .title .number-wish').text(numberitemW);
    });
  });
})();